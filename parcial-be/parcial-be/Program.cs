﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parcial_be
{
    class Program
    {
        static void Main(string[] args)
        {
            int anhos, fecha_anhos;
            string nombre, fecha;
            Console.Write("Ingrese su nombre:");
            nombre = Console.ReadLine();
            Console.Write("Ingrese su año de nacimiento:");
            fecha = Console.ReadLine();
            fecha_anhos = int.Parse(fecha);

            anhos = DateTime.Now.Year - fecha_anhos;
            Console.Write("Hola " + nombre + " disfruta de tus " + anhos + " años");

            Console.ReadKey();
        }   
    }
}
